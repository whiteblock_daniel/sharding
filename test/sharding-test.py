#!/usr/bin/env python

import os
import sys
import threading
import subprocess
import datetime 

bootNodes=1
clients=input("number of clients: ")
bootNodeIPList=[]
threads=[]

print(datetime.datetime.now())

# os.system('export DOCKER_API_VERSION="1.37"')

#Create containers with UMBA using sharding image
os.chdir('/home/appo/umba')
# os.system('./umba --i "sharding" --n '+str(bootNodes+clients))
os.system('./umba -i "sharding" -n '+str(bootNodes+clients))


def bootNodeDocker(i, bootIP):
    #Command to start boot node
    bootNodeCMD = 'docker exec -d whiteblock-node' +str(i)+ ' sharding-p2p-poc -seed=' +str(i)+ ' -port=5566 -rpcport=7788 -ip='+str(bootIP)

    print(bootNodeCMD)
    os.system(bootNodeCMD)

def clientNodeDocker(i, bootAddr, nodeIP):
    #Command to start sharding node
    clientNodeCMD = 'docker exec -d whiteblock-node' +str(i)+ ' sharding-p2p-poc \
        -seed='+str(i)+' \
        -port=5566 \
        -rpcport=7788 \
        -bootstrap \
        -bootnodes='+str(bootAddr)+' \
        -ip='+str(nodeIP)

    print(clientNodeCMD)
    os.system(clientNodeCMD)


for i in range(0,bootNodes):
    #Get container IP
    bootIPaddr = str(os.popen('docker inspect -f "{{ .NetworkSettings.Networks.'"wb_vlan" + str(i) +'.IPAddress }}" whiteblock-node'+str(i)).readline())

    bootIP = bootIPaddr.strip('\n')
    # print(ip)
    bootAddr = str('/ip4/'+bootIP+'/tcp/5566/ipfs/QmS5QmciTXXnCUCyxud5eWFenUMAmvAWSDa1c7dvdXRMZ7')
    bootNodeIPList.append(bootAddr)

    #Command to start boot node
    bootNodeDocker(i, bootIP)
    print('added boot node-'+str(i)+' at: '+str(bootIP))
    time.sleep(1)


for i in range(bootNodes,clients+bootNodes):
    nodeIPaddr = str(os.popen('docker inspect -f "{{ .NetworkSettings.Networks.'"wb_vlan" + str(i) +'.IPAddress }}" whiteblock-node'+str(i)).readline())

    IP = nodeIPaddr.strip('\n')

    bootAddr = bootNodeIPList[0]

    clientNodeDocker(i, bootAddr, IP)
    print('added client '+str(i)+' at: '+str(IP))
    time.sleep(1)

print(datetime.datetime.now())